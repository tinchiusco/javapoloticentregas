/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Persona;

/**
 *
 * @author tinch
 */
public class Mascota {
    
    private String nombre;
    private double peso;
    private Persona dueno;
    
    
    public Mascota(){
    
    };
    
    public Mascota(String nombre, double peso){
    this.nombre = nombre;
    this.peso = peso;
    
    };

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    /**
     * @return the dueno
     */
    public Persona getDueno() {
        return dueno;
    }

    /**
     * @param dueno the dueno to set
     */
    public void setDueno(Persona dueno) {
        this.dueno = dueno;
    }
}
