/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        String nombre;
        
        System.out.println("Ingrese su nombre");
        
        nombre = sc.next();
        
        System.out.println("Hola " + nombre + " un gusto conocerte");
        
    }
    
}
