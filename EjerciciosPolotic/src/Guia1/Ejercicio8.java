/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia1;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese la temperatura en Celsius para convertirla a Kelvin y Fahrenheit");
        
        float inputCelsius = sc.nextFloat();
        float kelvin = inputCelsius + 273.15f;
        float fahrenheit = (inputCelsius * 1.8f) + 32;
        
        System.out.println("Temperatura en Kelvin: " + kelvin + "\nTemperatura en Fahrenheit: " + fahrenheit);
        
        
    }
    
}
