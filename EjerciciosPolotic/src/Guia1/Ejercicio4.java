/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia1;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese la estatura de 3 personas");
        
        double persona1 = sc.nextFloat();
        double persona2 = sc.nextFloat();
        double persona3 = sc.nextFloat();
        double promedio = (persona1 + persona2 + persona3)/3;
        
        System.out.printf("El promedio es de %.2f", promedio);
        
        
        
    }
    
}
