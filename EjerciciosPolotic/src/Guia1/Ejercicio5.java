/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia1;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese el valor del radio del circulo");
        
        float radio = sc.nextFloat();
        double area = (double)Math.PI * Math.pow(radio, 2);
        double diametro = (double) 2 * Math.PI * radio;
        
        System.out.println("Los resultados son: \n Area: " + area + "\n Diametro: " + diametro);
        
        
        
        
    }
    
}
