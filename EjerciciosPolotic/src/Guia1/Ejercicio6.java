/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia1;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese el valor del producto");
        
        float precioProducto = sc.nextFloat();
        
        System.out.println("Ingrese el porcentaje de descuento a realizar");
        
        float descuentoPorcentual = sc.nextFloat();
        
        float descuentoTotal = precioProducto * (descuentoPorcentual/100);
        
        float precioFinal = precioProducto - descuentoTotal;
        
        System.out.println("Se hizo un descuento de " + descuentoTotal + "\n Precio final: " + precioFinal);
        
        
        
    }
    
}
