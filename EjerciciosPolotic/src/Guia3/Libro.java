/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia3;

import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class Libro {
    
    private double isbn;
    private String titulo, autor;
    private int paginas;
 
    public Libro(){};
    
    public Libro (double isbn, String titulo, String autor, int paginas){
    this.isbn = isbn;
    this.titulo = titulo;
    this.autor = autor;
    this.paginas = paginas;
    }
    
    public void setIsbn(double isbn){
    this.isbn = isbn;
    }
    
    public double getIsbn(){
    return isbn;
    }
    
    public void setTitulo(String titulo){
    this.titulo = titulo;
    }
    
    public String getTitulo(){
    return titulo;
    }
    
    public void setAutor(String autor){
    this.autor = autor;
    }
    
    public String getAutor(){
    return autor;
    }
    
    public void setPaginas(int paginas){
        this.paginas = paginas;
    }
    
    public int getPaginas(){
    return paginas;
    }
    
    public void getDatos(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Por favor ingrese datos del libro a ingresar \nCodigo ISBN");
        
        
        
    }
}
