/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia3.Ejercicio1;

import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class Libro {
    
    private long isbn;
    private String titulo, autor;
    private int paginas;
 
    public Libro(){};
    
    public Libro (long isbn, String titulo, String autor, int paginas){
    this.isbn = isbn;
    this.titulo = titulo;
    this.autor = autor;
    this.paginas = paginas;
    }
    
    public void setIsbn(long isbn){
    this.isbn = isbn;
    }
    
    public double getIsbn(){
    return isbn;
    }
    
    public void setTitulo(String titulo){
    this.titulo = titulo;
    }
    
    public String getTitulo(){
    return titulo;
    }
    
    public void setAutor(String autor){
    this.autor = autor;
    }
    
    public String getAutor(){
    return autor;
    }
    
    public void setPaginas(int paginas){
        this.paginas = paginas;
    }
    
    public int getPaginas(){
    return paginas;
    }
    
    public void setDatos(){
        Scanner sc = new Scanner(System.in);
        
        
        System.out.println("Por favor ingrese datos del libro a ingresar \nCodigo ISBN");
        long isbn = sc.nextLong();
        System.out.println("Titulo");
        String titulo = sc.next();
        System.out.println("Autor");
        String autor = sc.next();
        System.out.println("Numero de paginas");
        int paginas = sc.nextInt();
        
        setIsbn(isbn);
        setTitulo(titulo);
        setAutor(autor);
        setPaginas(paginas);
       
    }
    
    
    public String toString(){
    return "Libro cargado \nISBN: " + isbn + "\nTitulo: " + titulo + "\nAutor: " + autor + "\nPaginas: " + paginas;
    }
}
