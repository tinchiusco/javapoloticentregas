/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package MainPersonaMascota;

import Persona.Persona;
import Persona.Mascota;
import Persona.Perro;
import Persona.Gato;


public class MascotaPersona {
    
    public static void vincular (Persona dueno, Mascota mascota){
        dueno.setMascota(mascota);
        mascota.setDueno(dueno);
        }

    public static void main(String[] args) {
        
        
        
        Persona martin = new Persona("Martin", 36);
        Mascota mapache = new Mascota("Riquelme", 0.7);
        Perro wurst = new Perro("wurst", 15);
        Gato murphy = new Gato("Murphy", 1.5);
        
        vincular(martin, murphy);
        
        System.out.println(murphy.getNombre());
        System.out.println(murphy.getClass());
        System.out.println(murphy.getDueno().getNombre());
        System.out.println(martin.getMascota().getNombre());
    }
    
}
