/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia2;

import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class EjercicioPresencial2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
       
        System.out.println("Ingrese una palabra para saber si es un palidromo");
        
        String palabra = sc.nextLine().toLowerCase();
        
        String palabraInvertida= "";
        
        char caracter;
        
        for(int i = palabra.length() -1; i >= 0; i--){
            
            caracter = palabra.charAt(i);
            System.out.println(caracter);
            palabraInvertida += caracter;
        }
        
        System.out.println(palabraInvertida);
       
        if (palabra.equals(palabraInvertida)){
            System.out.println("Es palidromo");
        }else {
            System.out.println("No es palindromo");}
     
            
        }
        
    }
    

