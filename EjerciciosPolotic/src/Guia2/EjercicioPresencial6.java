/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class EjercicioPresencial6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       
        
       for (int numero = 1; numero <= 200; numero++) {
            boolean esPrimo = true;

            // Verifico si el número es primo
            for (int i = 2; i <= numero / 2; i++) {
                if (numero % i == 0) {
                    esPrimo = false;
                    break;
                }
            }

            // Imprimo el resultado
            if (esPrimo) {
                System.out.println(numero + " es primo.");
            } else {
                System.out.println(numero + " no es primo.");
            }
        }
    }
}
    

