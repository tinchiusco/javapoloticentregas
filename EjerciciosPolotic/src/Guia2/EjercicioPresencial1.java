/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia2;

import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class EjercicioPresencial1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese un numero para ver su tabla de multiplicar");
        
        int numero = sc.nextInt();
        
        for (int i = 0; i <= 10; i++){
        int resultado;
        
        resultado = numero * i;
        
            System.out.println(numero + "x" + i + "= " + resultado);
        }
    }
    
}
