/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Guia2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author tinch
 */
public class EjercicioPresencial5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
       
        System.out.println("Ingrese un numero para ver la sucesion de fibonacci");
        
        int numero = sc.nextInt();
        
        int resultado = 0;
        int sucesion = 1;
        int resultadoAnterior;
        
        for (int i = 0; i < numero; i++ ){
            
            resultadoAnterior = resultado;
            resultado = resultadoAnterior + sucesion;
            sucesion = resultadoAnterior;
            
            System.out.println(sucesion);
           
            
        }
       
        
        
     
            
        }
        
    }
    

